#coding:utf-8
import torch
import torch.nn as nn
import torch.nn.functional as F
from data_loader import *
import os
import pdb
# modelarts 平台上使用自动同步功能，可打开此句
#from modelarts.session import Session
import sys

os.environ['KMP_DUPLICATE_LIB_OK']='True'

#session = Session()
       
#session.upload_data(bucket_path=sys.argv[1], path=sys.argv[2])
def save2obs(localfile):
    # modelarts 平台上使用自动同步功能，可打开此句
    #session=Session()
    #session.upload_data(bucket_path="your bucket_path/pth",path=localfile)
    pass

def print_other(*msg):
    pass
    return 0
def print_loss(msg):
    print(msg)
    return 0
class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(1, 64,(3,3),(1,1),(1,1))
        self.pool1 = nn.MaxPool2d((2,2), (2,2),(0,0))
        self.conv2 = nn.Conv2d(64, 128,(3,3),(1,1),(1,1))
        self.pool2 = nn.MaxPool2d((2,2),(2,2),(0,0))
        self.conv3 = nn.Conv2d(128, 256,(3,3),(1,1),(1,1))
        self.conv3_bn = nn.BatchNorm2d(256)
        self.conv4 = nn.Conv2d(256,256,(3,3),(1,1),(1,1))
        self.pool3 = nn.MaxPool2d((2,1),(2,1),(0,0))
        self.conv5 = nn.Conv2d(256,512,(3,3),(1,1),(1,1))
        self.conv5_bn = nn.BatchNorm2d(512)
        self.pool4 = nn.MaxPool2d((1,2),(1,2),(0,0))
        self.conv6 = nn.Conv2d(512,512,(3,3),(1,1),(1,1))
        self.conv6_bn = nn.BatchNorm2d(512)
    def forward(self, x):
        x = self.pool1(F.relu(self.conv1(x)))
        #x = self.pool1(x)
        x = self.pool2(F.relu(self.conv2(x)))
        #x = self.pool2(x)
        x = F.relu(self.conv3_bn(self.conv3(x)))
        x = self.pool3(F.relu(self.conv4(x)))
        #x = self.pool3(x)
        x =self.pool4(F.relu(self.conv5_bn(self.conv5(x))))
        #x = self.pool4(x)
        x = F.relu(self.conv6_bn(self.conv6(x)))
        return x

class Encoder(nn.Module):
    def __init__(self, input_size, hidden_size,device):
        super(Encoder, self).__init__()
        self.hidden_size = hidden_size
        self.device = device
        print("encode device:%s"%(device))
        self.lstm = nn.LSTM(input_size, hidden_size,batch_first=True)
        if self.device=='cuda':
            self.lstm = self.lstm.cuda()
        
    def forward(self, x):
        # x -> (batch,length, encoder_input_size)； CNN_model return x
        batch_size = x.shape[0]
        h_init = torch.zeros(1,batch_size,self.hidden_size,device=self.device)
        c_init = torch.zeros(1,batch_size,self.hidden_size,device=self.device)
        outputs, (hn, cn) = self.lstm(x, (h_init,c_init))
        # outputs: ecnoder hiddens -> (batch, length, encoder_hidden_size)
        # hn, cn -> (#layers, batch, encoder_hidden_size)
        return outputs, (hn,cn)

import torch.nn.functional as F

class Decoder(nn.Module):
    def __init__(self, input_size, hidden_size, output_size, device):
        super(Decoder, self).__init__()
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.device = device
        self.lstm = nn.LSTM(input_size, hidden_size, batch_first=True)
        self.out = nn.Linear(hidden_size, output_size, bias=False)
        if self.device=='cuda':
            self.lstm=self.lstm.cuda()
            self.out=self.out.cuda()
    def forward(self, x, hp, cp):
        # x -> (batch, 1, input_size)
        # op -> (batch, hidden_size)
        # hp, cp -> (#layers, batch, hidden_size)
        # encoder_outputs -> (batch, time, #directions*encoder_hidden_size)
        #lstm_input = torch.cat([x, op.unsqueeze(dim=1)], dim=2) # (batch, 1, input_size + hidden_size)

        hidden, (hn, cn) = self.lstm(x, (hp, cp))
        #import pdb
        #pdb.set_trace()
        Y = torch.tanh(hidden.squeeze(1))
        P = F.log_softmax(self.out(Y), dim=1)
        # P: log softmax of symbol scores -> (batch, output_size)

        return P, (hn, cn)

def gen_data():
    #建立字典
    vocab = Vocab()
    print_other('Data loading... ')
    #用字典进行编码
    data = data_loader(vocab)
    image_train, formula_train, formulas,epoch_ended = data_loader.get_next_batch(data)
    #print_other('images_size:', image_train.shape)
    #print_other('formulas_size:', formula_train.shape)
    #for i in range(0,len(formulas)):
    #    print_other('formula_tensor[%s]:%s'%(len(formula_train[i]),formula_train[i]))
    #    print_other('formula[%s]:%s'%(len(formulas[i]),formulas[i]))
    #    print_other(50*"-")


def sgd(weight,learning_rate):
    # 出现nan时，将梯度设置为0
    if torch.isnan(torch.sum(weight.grad.data)):
        weight.grad.data.zero_()
    # 请在这里对weight实现sgd
    
def one_hot(token_start,length):
    one_hot=torch.zeros(token_start.shape[0],length).int()
    for i in range(token_start.shape[0]):
        one_hot[i,token_start[i].item()]=1
    return one_hot

# 模型inference
def im2latex_train(image_train,formula_train,cnn,encode,decode,loss,vocab,device):
    B=len(image_train)
    #rand_image = torch.randn(B, 1, 256, 256)
    
    image_train = torch.from_numpy(image_train)
    
    if device=='cuda':
        image_train=image_train.cuda()
    #input_image = torch.tensor(image_train, dtype=torch.float32)
    input_image = image_train.clone().detach()
    input_image = input_image.float()
    print_other("-"*20)
    print_other("cnn")
    #cnn=CNN()
    print_other("in_cnn_fea_shape",input_image.shape)
    feature=cnn(input_image)
    # 2,512,32,32
    print_other("cnn_out_fea_shape",feature.shape)
    print_other("-"*20)
    print_other("mat to vector")
    B, C, H_prime, W_prime = feature.shape
    feature = feature.permute(0, 2, 3, 1).contiguous().view(B,H_prime*W_prime, C) # (batch,  H'*W', C)
    #print_other(feature.shape)
    print_other("map2seq_fea_shape:",feature.shape)
    print_other("-"*20)
    print_other("encoder:")
    #encode = Encoder(512,512,"cpu")
    encoder_outputs,(ehn,ecn) = encode(feature)  
    print_other("encoder hn shape:",ehn.shape)
    print_other("-"*20)

    print_other("decoder:")
    #decode= Decoder(80,512,80,"cpu")

    token_start =torch.zeros(B).type(torch.uint8)
    #token_start = torch.tensor([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
    print_other(token_start)
    #token_vector=torch.nn.functional.one_hot(token_start, 422)
    token_vector=one_hot(token_start, 422)
    if device=='cuda':
        token_vector=token_vector.cuda()
    logits=[]
    preds=[]
    formulas=[]
    for fi in range(B):
        formulas.append("")
    #formulas=["","","","","","","","","","","","","","","","","","","","","","","","","","","",]
    for i in range(0,B):
         token_numpy =token_start.cpu().numpy()
         formulas[i]+=vocab.idx2token[token_numpy[i]]
    total_loss=0.
    for t in range(0,36):
        P,(dhn,dcn)=decode(token_vector.view(B,1,422).float(),ehn,ecn)
        #import pdb
        #pdb.set_trace()
        gt=torch.tensor(formula_train[:,t])
        if device=='cuda':
            gt=gt.cuda()
        ## 请这里使用loss函数产生loss value,注意上下文，要对哪两个变量求loss

        loss_value = 
        
        if torch.isnan(loss_value):
            import pdb
            print("nan.....")
            pdb.set_trace()
        total_loss+=loss_value.detach()
        loss_value.backward(retain_graph=True)
        # sgd learning rate = 0.000001
        learning_rate=0.002
        '''
        # decode
        decode.out.weight.data.sub_(learning_rate*decode.out.weight.grad.data)
        decode.lstm.weight_ih_l0.data.sub_(learning_rate*decode.lstm.weight_ih_l0.grad.data)
        decode.lstm.weight_hh_l0.data.sub_(learning_rate*decode.lstm.weight_hh_l0.grad.data)
        decode.lstm.bias_ih_l0.data.sub_(learning_rate*decode.lstm.bias_ih_l0.grad.data)
        decode.lstm.bias_hh_l0.data.sub_(learning_rate*decode.lstm.bias_hh_l0.grad.data)
        #encode 
        encode.lstm.weight_ih_l0.data.sub_(learning_rate*encode.lstm.weight_ih_l0.grad.data)
        encode.lstm.weight_hh_l0.data.sub_(learning_rate*encode.lstm.weight_hh_l0.grad.data)
        encode.lstm.bias_ih_l0.data.sub_(learning_rate*encode.lstm.bias_ih_l0.grad.data)
        encode.lstm.bias_hh_l0.data.sub_(learning_rate*encode.lstm.bias_hh_l0.grad.data)
        # cnn weights
        cnn.conv1.weight.data.sub_(learning_rate*cnn.conv1.weight.grad.data)
        cnn.conv2.weight.data.sub_(learning_rate*cnn.conv2.weight.grad.data)
        cnn.conv3.weight.data.sub_(learning_rate*cnn.conv3.weight.grad.data)
        cnn.conv3_bn.weight.data.sub_(learning_rate*cnn.conv3_bn.weight.grad.data)
        cnn.conv4.weight.data.sub_(learning_rate*cnn.conv4.weight.grad.data)
        cnn.conv5.weight.data.sub_(learning_rate*cnn.conv5.weight.grad.data)
        cnn.conv5_bn.weight.data.sub_(learning_rate*cnn.conv5_bn.weight.grad.data)
        cnn.conv6.weight.data.sub_(learning_rate*cnn.conv6.weight.grad.data)
        cnn.conv6_bn.weight.data.sub_(learning_rate*cnn.conv6_bn.weight.grad.data)
        #cnn bias
        cnn.conv1.bias.data.sub_(learning_rate*cnn.conv1.bias.grad.data)
        cnn.conv2.bias.data.sub_(learning_rate*cnn.conv2.bias.grad.data)
        cnn.conv3.bias.data.sub_(learning_rate*cnn.conv3.bias.grad.data)
        cnn.conv3_bn.bias.data.sub_(learning_rate*cnn.conv3_bn.bias.grad.data)
        cnn.conv4.bias.data.sub_(learning_rate*cnn.conv4.bias.grad.data)
        cnn.conv5.bias.data.sub_(learning_rate*cnn.conv5.bias.grad.data)
        cnn.conv5_bn.bias.data.sub_(learning_rate*cnn.conv5_bn.bias.grad.data)
        cnn.conv6.bias.data.sub_(learning_rate*cnn.conv6.bias.grad.data)
        cnn.conv6_bn.bias.data.sub_(learning_rate*cnn.conv6_bn.bias.grad.data)

        # 梯度清零
        # decode
        decode.out.weight.grad.data.zero_()
        decode.lstm.weight_ih_l0.grad.data.zero_()
        decode.lstm.weight_hh_l0.grad.data.zero_()
        decode.lstm.bias_ih_l0.grad.data.zero_()
        decode.lstm.bias_hh_l0.grad.data.zero_()
        #encode 
        encode.lstm.weight_ih_l0.grad.data.zero_()
        encode.lstm.weight_hh_l0.grad.data.zero_()
        encode.lstm.bias_ih_l0.grad.data.zero_()
        encode.lstm.bias_hh_l0.grad.data.zero_()
        # cnn weights
        cnn.conv1.weight.grad.data.zero_()	
        cnn.conv2.weight.grad.data.zero_()	
        cnn.conv3.weight.grad.data.zero_()	
        cnn.conv3_bn.weight.grad.data.zero_()	
        cnn.conv4.weight.grad.data.zero_()	
        cnn.conv5.weight.grad.data.zero_()	
        cnn.conv5_bn.weight.grad.data.zero_()	
        cnn.conv6.weight.grad.data.zero_()	
        cnn.conv6_bn.weight.grad.data.zero_()	
        #cnn bias
        cnn.conv1.bias.grad.data.zero_()	
        cnn.conv2.bias.grad.data.zero_()	
        cnn.conv3.bias.grad.data.zero_()	
        cnn.conv3_bn.bias.grad.data.zero_()	
        cnn.conv4.bias.grad.data.zero_()	
        cnn.conv5.bias.grad.data.zero_()	
        cnn.conv5_bn.bias.grad.data.zero_()	
        cnn.conv6.bias.grad.data.zero_()	
        cnn.conv6_bn.bias.grad.data.zero_()	

        # 梯度清零
        #decode.weights.grad.data.zero_()
        #encode.weights.grad.data.zero_()
        #cnn.weights.grad.data.zero_()
        # 梯度清零
        #decode.weights.grad.data.zero_()
        #encode.weights.grad.data.zero_()
        #cnn.weights.grad.data.zero_()
        '''
        
        # sgd
        # decode
        sgd(decode.out.weight,learning_rate)
        sgd(decode.lstm.weight_ih_l0,learning_rate)
        sgd(decode.lstm.weight_hh_l0,learning_rate)
        sgd(decode.lstm.bias_ih_l0,learning_rate)
        sgd(decode.lstm.bias_hh_l0,learning_rate)
        #encode 
        sgd(encode.lstm.weight_ih_l0,learning_rate)
        sgd(encode.lstm.weight_hh_l0,learning_rate)
        sgd(encode.lstm.bias_ih_l0,learning_rate)
        sgd(encode.lstm.bias_hh_l0,learning_rate)
        # cnn weights
        sgd(cnn.conv1.weight,learning_rate)
        sgd(cnn.conv2.weight,learning_rate)
        sgd(cnn.conv3.weight,learning_rate)
        sgd(cnn.conv3_bn.weight,learning_rate)
        sgd(cnn.conv4.weight,learning_rate)
        sgd(cnn.conv5.weight,learning_rate)
        sgd(cnn.conv5_bn.weight,learning_rate)
        sgd(cnn.conv6.weight,learning_rate)
        sgd(cnn.conv6_bn.weight,learning_rate)
        #cnn bias
        sgd(cnn.conv1.bias,learning_rate)
        sgd(cnn.conv2.bias,learning_rate)
        sgd(cnn.conv3.bias,learning_rate)
        sgd(cnn.conv3_bn.bias,learning_rate)
        sgd(cnn.conv4.bias,learning_rate)
        sgd(cnn.conv5.bias,learning_rate)
        sgd(cnn.conv5_bn.bias,learning_rate)
        sgd(cnn.conv6.bias,learning_rate)
        sgd(cnn.conv6_bn.bias,learning_rate)
        
        
        
        logits.append(P)
        
        # 
        next_token_pre = P.argmax(dim=1)
        for i in range(0,B):
             token_numpy =next_token_pre.cpu().numpy()
             formulas[i]+=vocab.idx2token[token_numpy[i]]
        preds.append(next_token_pre)
        
        # teacher_forcing,用当前时刻的ground truch生成下一时刻的输入值token_vector
         
        token_vector=
        
        if device=='cuda':
            token_vector=token_vector.cuda()
        preds.append(next_token_pre)
        #if next_token_pre.data==troch.tensor(2):
        #    break
        #import pdb
        #pdb.set_trace()
    #增加start_token的概率值 
    if device=='cuda':
        logits = [torch.zeros(logits[1].shape[0], logits[1].shape[1]).cuda()] + logits
    else:
        logits = [torch.zeros(logits[1].shape[0], logits[1].shape[1])] + logits
    
    # 将start_token对应的位置设置为1
    # modelarts上，python 1.0.0版本时，用此句
    #logits[0][:, token_start.numpy()] = 1
    # mac pytorch 1.5.0
    logits[0][:, token_start.numpy().tolist()] = 1
    # 列表变tensor
    logits = torch.stack(logits, dim=1)
    # 列表变tensor
    preds = torch.stack(preds, dim=1)
    #pdb.set_trace()
    print_other("-"*20)
    print_other(formulas[0])
    print_other("total_loss=%s"%(total_loss))
    return total_loss,formulas
    


if __name__=="__main__":
    device='cpu'
    if torch.cuda.is_available():
        device='cuda'
        print("u have [%s] gpus"%(torch.cuda.device_count()))
    
    vocab = Vocab()
    data  = data_loader(vocab)
    image_train,formula_train,formulas,epoch_ended = data_loader.get_next_batch(data)
    formula_train = formula_train.astype('int64')
    #gt=torch.nn.functional.one_hot(torch.tensor(formula_train),422).float()
    #import pdb
    #pdb.set_trace()
    cnn=CNN()
    encode = Encoder(512,512,device)
    decode= Decoder(422,512,422,device) 
    if device=='cuda':    
        cnn_gpu=CNN().cuda()
        encode_gpu=encode.cuda()
        decode_gpu=decode.cuda()
        #if os.path.exists("./pth/cnn_encoder_decoder.pth"):
        #    checkpoint = torch.load('./pth/cnn_encoder_decoder.pth')
        #    cnn_gpu.load_state_dict(checkpoint['cnn'])
        #    encode_gpu.load_state_dict(checkpoint['encoder'])
        #    decode_gpu.load_state_dict(checkpoint['decoder'])
    # 请在这里定义loss函数,方便在389行使用    
    loss = 
    epoch=0
    for iter_batch  in range(0,10000):
        if device=='cuda':
            iter_loss,formulas_pred=im2latex_train(image_train,formula_train,cnn_gpu,encode_gpu,decode_gpu,loss,vocab,device)
        else:
            iter_loss,formulas_pred=im2latex_train(image_train,formula_train,cnn,encode,decode,loss,vocab,device)
        image_train,formula_train,formulas,epoch_ended = data_loader.get_next_batch(data)
        formula_train = formula_train.astype('int64')
        #if device=='cuda':
        if device=='cpu':
            if iter_batch%2==0:  
                print_loss("-"*20)
                print_loss("batch/epoch=[%s/%s],loss=%s"%(iter_batch,epoch,iter_loss))
                fpredi=0
                for fpred in formulas_pred:
                    print_loss("test formula [%s]:%s"%(fpredi,fpred))
                    fpredi+=1
            #if epoch%5==1:
	    if iter_batch%5==0:
                print_loss("batch/epoch=[%s/%s],loss=%s"%(iter_batch,epoch,iter_loss))
                state = {'cnn':cnn.state_dict(),'encoder':encode.state_dict(),'decoder':decode.state_dict()}
                torch.save(state, './pth/cnn_encoder_decoder.pth')
                save2obs('./pth/cnn_encoder_decoder.pth')
                
        else:
                print_loss("batch/epoch=[%s/%s],loss=%s"%(iter_batch,epoch,iter_loss))
            
        if epoch_ended:
            epoch=epoch+1
        
