import torch
import torch.nn as nn
import pdb
pdb.set_trace()
rnn = nn.LSTM(10, 20, 2)
x1 = torch.randn(5, 3, 10)
h0 = torch.randn(2, 3, 20)
c0 = torch.randn(2, 3, 20)
output, (hn, cn) = rnn(x1, (h0, c0))


##
pdb.set_trace()
rnn = nn.LSTM(10, 20, banch_fisrt=True)
# 5:seq,3,batch,10,dimension
x1 = torch.randn(3, 2250, 10)
h0 = torch.randn(1, 3, 20)
c0 = torch.randn(1, 3, 20)
output, (hn, cn) = rnn(x1, (h0, c0))
